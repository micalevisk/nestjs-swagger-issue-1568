import { PickType as PickTypeMappedTypes } from '@nestjs/mapped-types';
import { PickType } from '@nestjs/swagger';

export class Foo {
  name: string;

  constructor(name: string) {
    this.name = name; 
  }
}

export class Bar extends PickType(Foo, ['name'] as const) {}
export class Baz extends PickTypeMappedTypes(Foo, ['name'] as const) {}


const bar = new Bar('bar');
const baz = new Baz('baz');

console.log('bar.name', bar.name) // prints `undefined`
console.log('baz.name', baz.name) // prints `undefined`

